#!/bin/bash

for i in {1..99}
do
    REMAINDER=$(( $i % 2 ))
    if [ $REMAINDER -ne 0 ]
    then
        echo "$i"
    fi
done