#!/bin/bash

read letter

if [[ $letter =~ [yY] ]]
then
    echo "YES"
elif [[ $letter =~ [nN] ]]
then
    echo "NO"
fi