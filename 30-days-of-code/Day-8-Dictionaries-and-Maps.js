module.exports = function (arr1, arr2, n) {
  let dict = {};

  for (let i = 0; i < n; i++) {
    const splitString = arr1[i].split(" ");
    dict[String(splitString[0])] = String(splitString[1]);
  }

  for (let i = 0; i < n; i++) {
    const lookName = arr2[i];
    if (dict[lookName]) console.log(`${lookName}=${dict[lookName]}`);
    else console.log("Not found");
  }
};
