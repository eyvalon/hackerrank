#!/bin/python3

class Node:
	def __init__(self, data):
		self.data = data
		self.next = None 

class Solution: 
	def display(self, head):
		current = head

		while current:
			print(current.data,end=' ')
			current = current.next
		
		print()

	def __init__(self):
		self.currentHead = None

	def insert(self, head, data):
		print(data)
		data = Node(data)
		head = data
		data.next = self.currentHead

		return head
		
	#Complete this method

mylist= Solution()
T=int(input())
head=None

for i in range(T):
	data=int(input())
	head=mylist.insert(head, data) 
	
mylist.display(head); 	  