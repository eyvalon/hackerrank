// Function = meal_cost + meal_cost *(tip_percent / 100) + meal_cost * (tax_percent / 100)
module.exports = function (N) {
  let returnString = "";

  if (N % 2 != 0) {
    returnString += "Weird";
  } else {
    if (N > 1 && N < 6) {
      returnString += "Not Weird";
    } else if (N > 5 && N < 21) {
      returnString += "Weird";
    } else {
      returnString += "Not Weird";
    }
  }

  return returnString;
};
