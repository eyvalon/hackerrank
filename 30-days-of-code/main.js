// const solve = require("./Day-2-Operators");
// const isWeird = require("./Day-3-Intro-To-Conditional-Statements");
// const Person = require("./Day-4-Class-vs-Instance");
// const loopTimes = require("./Day-5-Loops");
// const evenOddLetters = require("./Day-6-Lets-Review");
// const reverseString = require("./Day-7-Arrays.js");
// const createDictionary = require("./Day-8-Dictionaries-and-Maps");
// const factorial = require("./Day-9-Recursion");
// const convertToBinary = require("./Day-10-Binary-Numbers");
// const calculateSum = require("./Day-11-2D-Arrays");

("use strict");

process.stdin.resume();
process.stdin.setEncoding("utf-8");

let inputString = "";
let currentLine = 0;

process.stdin.on("data", (inputStdin) => {
  inputString += inputStdin;
});

process.stdin.on("end", (_) => {
  inputString = inputString
    .trim()
    .split("\n")
    .map((string) => {
      return string.trim();
    });

  main();
});

function readLine() {
  return inputString[currentLine++];
}

function main() {
  let result;

  // // Day 1: Data Types
  // var i = 4;
  // var d = 4.0;
  // var s = "HackerRank ";
  // // Declare second integer, double, and String variables.
  // let one = 0;
  // let two = 0.0;
  // let three = "";

  // // Read and save an integer, double, and String to your variables.
  // one = readLine();
  // two = readLine();
  // three = readLine();

  // // Print the sum of both integer variables on a new line.
  // let sumInt = i + Number(one);
  // console.log(sumInt);

  // // Print the sum of the double variables on a new line.
  // let sumFloat = d + parseFloat(two);
  // console.log(sumFloat.toFixed(1));

  // // Concatenate and print the String variables on a new line
  // // The 's' variable above should be printed first.
  // let concatString = s.concat(three);
  // console.log(concatString);

  // Day 2: Operators
  // const meal_cost = parseFloat(readLine());
  // const tip_percent = parseInt(readLine(), 10);
  // const tax_percent = parseInt(readLine(), 10);
  // result = solve(meal_cost, tip_percent, tax_percent);

  // Day 3: Conditional Statements
  // const N = parseInt(readLine(), 10);
  // result = isWeird(N);

  // Day 4: Class vs. Instance
  // let T = parseInt(readLine());
  // for (i = 0; i < T; i++) {
  //   const age = parseInt(readLine());
  //   const p = new Person(age);
  //   p.amIOld();
  //   for (j = 0; j < 3; j++) {
  //     p.yearPasses();
  //   }
  //   p.amIOld();
  //   console.log("");
  // }

  // Day 5: Loops
  // const n = parseInt(readLine(), 10);
  // loopTimes(n);

  // Day 6: Let's Review
  // let T = parseInt(readLine());
  // for (i = 0; i < T; i++) {
  //   const currentWord = readLine();
  //   evenOddLetters(currentWord);
  // }

  // Day 7: Arrays
  // const n = parseInt(readLine(), 10);
  // const arr = readLine()
  //   .split(" ")
  //   .map((arrTemp) => parseInt(arrTemp, 10));

  // reverseString(arr, n);

  // Day 8: Dictionaries and Maps
  // let T = parseInt(readLine());

  // let arr1 = [];
  // let arr2 = [];

  // for (i = 0; i < T; i++) {
  //   const currentLine = readLine();
  //   arr1.push(currentLine);
  // }

  // for (i = 0; i < T; i++) {
  //   const currentLine = readLine();
  //   arr2.push(currentLine);
  // }

  // createDictionary(arr1, arr2, T);

  // Day 9: Recursion
  // const n = parseInt(readLine(), 10);
  // result = factorial(n);

  // Day 10: Binary Numbers
  // const n = parseInt(readLine(), 10);
  // result = convertToBinary(n);

  // Day 11: 2D Arrays
  // let arr = Array(6);

  // for (let i = 0; i < 6; i++) {
  //   arr[i] = readLine()
  //     .split(" ")
  //     .map((arrTemp) => parseInt(arrTemp, 10));
  // }

  // result = calculateSum(arr, arr.length);
  // console.log(result);
}
