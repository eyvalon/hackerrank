// Function = meal_cost + meal_cost *(tip_percent / 100) + meal_cost * (tax_percent / 100)
module.exports = function (initialAge) {
  this.currentAge = -1;
  this.currentAge = initialAge;

  // Add some more code to run some checks on initialAge
  // Do some computations in here and print out the correct statement to the console
  this.amIOld = function () {
    if (this.currentAge < 0) {
      console.log("Age is not valid, setting age to 0.\nYou are young.");
      this.currentAge = 0;
    } else {
      if (this.currentAge < 13) {
        console.log("You are young.");
      } else if (this.currentAge >= 13 && this.currentAge < 18) {
        console.log("You are a teenager.");
      } else {
        console.log("You are old.");
      }
    }
  };

  // Increment the age of the person in here
  this.yearPasses = function () {
    this.currentAge += 1;
  };
};
