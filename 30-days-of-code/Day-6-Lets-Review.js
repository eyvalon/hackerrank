module.exports = function (word) {
  let splitWord = word.split("");

  const [oddList, evenList] = splitWord.reduce(
    ([odd, even], e) =>
      e % 2 == 0 ? [[...odd, e], even] : [even, [...odd, e]],
    [[], []]
  );

  console.log(`${oddList.join("")} ${evenList.join("")}`);
};
