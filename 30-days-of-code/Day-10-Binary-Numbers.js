module.exports = function (n) {
  let currentString = n.toString(2).split("");
  let consecutiveTimes = 0;
  let counter = 0;

  for (let i = 0; i < currentString.length; i++) {
    if (currentString[i] == "1") {
      counter++;
      if (counter > consecutiveTimes) {
        consecutiveTimes = counter;
      }
    } else {
      counter = 0;
    }
  }

  return consecutiveTimes;
};
