module.exports = function (arr) {
  let sum = new Array(16).fill(0);
  let increment = 0;

  for (let i = 0; i < 4; i++) {
    for (let ii = 0; ii < 4; ii++) {
      let current_sum =
        arr[i][ii] +
        arr[i][ii + 1] +
        arr[i][ii + 2] +
        arr[i + 1][ii + 1] +
        arr[i + 2][ii] +
        arr[i + 2][ii + 1] +
        arr[i + 2][ii + 2];

      sum[increment] = current_sum;
      increment += 1;
    }
  }

  return Math.max.apply(Math, sum);
};
