// Function = meal_cost + meal_cost *(tip_percent / 100) + meal_cost * (tax_percent / 100)
module.exports = function (meal_cost, tip_percent, tax_percent) {
  let total_amount = 0;

  const tax_amount = eval((meal_cost * tax_percent) / 100);
  const tip_amount = eval((meal_cost * tip_percent) / 100);

  total_amount = Math.round(meal_cost + tax_amount + tip_amount);

  return total_amount;
};
