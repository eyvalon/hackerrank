const counter = (function counter() {
  let value = 0;
  return {
    getValue: function () {
      return value;
    },
    changeBy: function (k) {
      value += k;
    }
  };
})();

module.exports = function (k) {
  let value = counter;
  return {
    increment: () => {
      value.changeBy(k);
    },
    decrement: () => {
      value.changeBy(-k);
    },
    getValue: () => {
      return value.getValue();
    }
  };
};
