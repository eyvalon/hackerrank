/*
 * Complete the 'fizzBuzz' function below.
 *
 * The function accepts INTEGER n as parameter.
 */
module.exports = function (n) {
  for (let i = 1; i < n + 1; i++) {
    let currentString = "";
    if (i % 3 == 0) {
      currentString += "Fizz";
    }

    if (i % 5 == 0) {
      currentString += "Buzz";
    }

    if (i % 5 != 0 && i % 3 != 0) {
      currentString += i;
    }

    console.log(currentString);
  }
};
