// const fizzBuzz = require("./FizzBuzz");
// const getFixedCounter = require("./Step-Counter");
const getTeams = require("./Champion-Leagues");

("use strict");

const assert = require("assert");

process.stdin.resume();
process.stdin.setEncoding("utf-8");

let inputString = "";
let currentLine = 0;

process.stdin.on("data", (inputStdin) => {
  inputString += inputStdin;
});

process.stdin.on("end", (_) => {
  inputString = inputString
    .trim()
    .split("\n")
    .map((string) => {
      return string.trim();
    });

  main();
});

function readLine() {
  return inputString[currentLine++];
}

async function main() {
  // Fizz Buzz (Example)
  // const n = parseInt(readLine().trim(), 10);
  // fizzBuzz(n);

  // const val = parseInt(readLine().trim());
  // const c = getFixedCounter(val);
  // assert(!("changeBy" in c));

  // const n = parseInt(readLine().trim());
  // for (let i = 0; i < n; ++i) {
  //   const op = readLine().trim();
  //   if (op == "+") {
  //     c.increment();
  //   } else if (op == "-") {
  //     c.decrement();
  //   } else if (op === "?") {
  //     // ws.write(`${c.getValue()}\n`);
  //   }
  // }

  // Champion-League
  const year = parseInt(readLine().trim());
  const k = parseInt(readLine().trim());

  const teams = await getTeams(year, k);

  for (const team of teams) {
    console.log(team);
  }
}
