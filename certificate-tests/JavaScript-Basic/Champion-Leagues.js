const https = require("https");

module.exports = async function (year, k) {
  console.log(year, k);
  // write your code here
  // API endpoint template: https://jsonmock.hackerrank.com/api/football_matches?competition=UEFA%20Champions%20League&year=<YEAR>&page=<PAGE_NUMBER>
  return new Promise((resolve, reject) => {
    https.get(
      `https://jsonmock.hackerrank.com/api/football_matches?competition=UEFA%20Champions%20League&year=${year}&page=${k}`,
      (response) => {
        response.setEncoding("utf8");
        let rawData = [];

        response.on("data", (chunk) => {
          rawData += chunk;
        });

        response.on("end", function () {
          const parsedData = JSON.parse(rawData);
          resolve(parsedData.data);
        });
      }
    );
  });
};
