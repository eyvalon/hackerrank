#!/bin/python3

if __name__ == '__main__':
	scoreList = []

	for _ in range(int(input())):
		name = input()
		score = float(input())
		scoreList.append([name, score])

	sortedNames = [x for x in sorted(scoreList, key=lambda val:val[0])]

	uniqueScores = []
	[uniqueScores.append(x[1]) for x in sorted(scoreList, key=lambda val:val[1]) if x[1] not in uniqueScores]

	secondLowest = (uniqueScores[1])

	[print(x[0]) for x in sortedNames if x[1] == secondLowest]