#!/bin/python3

if __name__ == '__main__':
	N,M = map(int, input().split(" "))
	repeatString = ".|."

	for i in range(N + 1):
		if(i == 0) or (i == N):
			print(repeatString.center(M, "-"))

		elif i < N // 2:
			print((repeatString + (repeatString*2)*(i)).center(M, "-"))

		elif i == ((N // 2) + 1):
			print("WELCOME".center(M, "-"))
		
		elif i > N // 2:
			print((repeatString + (repeatString*2)*((N//2) - ((i - 1) % (N//2)))).center(M, "-"))