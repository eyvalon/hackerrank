#!/bin/python3

if __name__ == '__main__':
	n = int(input())
	arr = map(int, input().split())

	uniqueArray = []
	
	[uniqueArray.append(x) for x in sorted(arr) if x not in uniqueArray]

	print(uniqueArray[-2])	

