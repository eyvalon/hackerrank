#!/bin/python3
import numpy

if __name__ == '__main__':
	N,M,P	= input().split(" ")

	arrN = []
	arrM = []

	for _ in range(int(N)):
		arrN.append(list(map(int, input().split(" "))))
		
	for _ in range(int(M)):
		arrM.append(list(map(int, input().split(" "))))
		


	print(numpy.concatenate((numpy.array(arrN), numpy.array(arrM)), axis = 0))