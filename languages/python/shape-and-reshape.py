#!/bin/python3

import numpy

if __name__ == '__main__':
	arr = list(map(int, input().split(" ")))

	print(numpy.reshape(numpy.array(arr), (3,3)))
