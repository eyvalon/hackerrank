#!/bin/python3

def solve(s):
	fullName = s.split(" ")
	capitaliseName = [x.capitalize() for x in fullName]

	return " ".join(capitaliseName)

if __name__ == '__main__':
	s = input()
	result = solve(s)
	print(result)