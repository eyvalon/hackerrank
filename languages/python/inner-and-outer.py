#!/bin/python3

import numpy

if __name__ == '__main__':
	aArray = input()
	bArray = input()

	A = aArray.split(" ")
	B = bArray.split(" ")

	A = list(map(int, A))
	B = list(map(int, B))
	# A = [int(x) for x in A]
	# B = [int(x) for x in B]

	inner = numpy.inner(numpy.array(A), numpy.array(B))
	outer = numpy.outer(numpy.array(A), numpy.array(B))

	print(inner)
	print(outer)