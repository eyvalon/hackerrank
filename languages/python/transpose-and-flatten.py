#!/bin/python3
import numpy

if __name__ == '__main__':
	arr = []
	N, M = input().split(" ")
	
	for _ in range(int(N)):
		arr.append(list(map(int, input().split(" "))))
		
	print(numpy.transpose(arr))
	print(numpy.array(arr).flatten())