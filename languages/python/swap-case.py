#!/bin/python3

def swap_case(s):
	swapString = ""
	for letter in s:
		if letter.isalpha():
			if (ord(letter)) < 97:
				swapString += (chr((ord(letter) + 32)))
			else:
				swapString += (chr((ord(letter) - 32)))
		else:
			swapString += letter

	return swapString

if __name__ == '__main__':
	s = input()
	result = swap_case(s)
	print(result)