class Calculator {
  constructor(values) {
    this.values = values;
    this.temp = "";
    this.dataArray = [];
    this.calculated = false;
  }

  addValue(value) {
    this.values += value;
    this.temp += value;

    if (this.calculated) {
      this.dataArray = [];
      this.temp = this.values;
      this.calculated = false;
    }

    this.render();
  }

  reset() {
    this.dataArray = [];
    this.temp = "";
  }

  method(type) {
    switch (type) {
      case "+":
      case "-":
      case "*":
      case "/":
        if (this.temp != "") {
          this.dataArray.push(parseInt(Number(this.temp), 2).toString(10));
          this.temp = "";
        }

        if (this.calculated) {
          this.calculated = false;
        }

        this.values += type;
        this.dataArray.push(type);
        break;

      case "=":
        if (!this.calculated) {
          let stringTemp = "";
          const temp_num = parseInt(Number(this.temp), 2).toString(10);
          this.dataArray.push(temp_num);

          for (let i = 0; i < this.dataArray.length; i++) {
            stringTemp += this.dataArray[i];
          }

          const currentNumber = eval(stringTemp).toString(2);
          this.values = currentNumber.toString();
          this.dataArray = [parseInt(currentNumber, 2).toString(10)];
          this.calculated = true;
          this.temp = "";
        }
        break;

      case "C":
        this.reset();
        this.values = "";
        break;

      default:
        // Error
        break;
    }

    this.render();
  }

  render() {
    document.getElementById("res").innerHTML = this.values;
  }
}

document.addEventListener("DOMContentLoaded", () => {
  const binaryCalculator = new Calculator("");
  binaryCalculator.render();

  document.getElementById("btn0").addEventListener("click", () => {
    binaryCalculator.addValue(0);
  });
  document.getElementById("btn1").addEventListener("click", () => {
    binaryCalculator.addValue(1);
  });

  document.getElementById("btnSum").addEventListener("click", () => {
    binaryCalculator.method("+");
  });
  document.getElementById("btnSub").addEventListener("click", () => {
    binaryCalculator.method("-");
  });
  document.getElementById("btnMul").addEventListener("click", () => {
    binaryCalculator.method("*");
  });
  document.getElementById("btnDiv").addEventListener("click", () => {
    binaryCalculator.method("/");
  });
  document.getElementById("btnEql").addEventListener("click", () => {
    binaryCalculator.method("=");
  });
  document.getElementById("btnClr").addEventListener("click", () => {
    binaryCalculator.method("C");
  });
});
