/**
 *   Return the second largest number in the array.
 *   @param {Number[]} nums - An array of numbers.
 *   @return {Number} The second largest number in the array.
 **/
module.exports = function (nums) {
  function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }

  const uniqueArray = nums.filter(onlyUnique);
  const sortedArrays = uniqueArray.sort();

  var largest = sortedArrays[0];
  var secondLargest = 0;

  for (var i = 0; i < sortedArrays.length; i++) {
    if (sortedArrays[i] > largest) {
      secondLargest = largest;

      largest = sortedArrays[i];
    } else if (sortedArrays[i] > secondLargest && sortedArrays[i] !== largest) {
      secondLargest = sortedArrays[i];
    }
  }

  return secondLargest;
};
