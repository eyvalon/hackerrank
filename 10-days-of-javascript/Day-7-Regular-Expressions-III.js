/*
 * Declare a RegExp object variable named 're'
 * It must match ALL occurrences of numbers in a string.
 */
module.exports = function () {
  let re = /[+-]?\d+(?:\\d+)?/g;
  /*
   * Do not remove the return statement
   */
  return re;
};
