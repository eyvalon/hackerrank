/*
 * Declare a RegExp object variable named 're'
 * It must match a string that starts with 'Mr.', 'Mrs.', 'Ms.', 'Dr.', or 'Er.',
 * followed by one or more letters.
 */
module.exports = function () {
  let regex = /^(Mr\.|Mrs\.|Ms\.|Dr\.|Er\.)([a-zA-Z])+$/g;
  let re = new RegExp(regex);

  /*
   * Do not remove the return statement
   */
  return re;
};
