// const greeting = require("./Day-0-Hello-World");
// const performOperation = require("./Day-0-Data-Types");
// const Arithmetrics = require("./Day-1-Arithmetric-Operators");
// const Factorial = require("./Day-1-Functions");
// const vowelsAndConsonants = require("./Day-2-Loops");
// const getLetter = require("./Day-2-Conditional-Statements-Switch");
// const getGrade = require("./Day-2-Conditional-Statements-If-Else");
// const reverseString = require("./Day-3-Try-Catch-and-Finally");
// const getSecondLargest = require("./Day-3-Arrays");
// const isPositive = require("./Day-3-Throw");
// const Rectangle = require("./Day-4-Create-A-Rectangle-Object");
// const getCount = require("./Day-4-Count-Objects");
// const modifyArray = require("./Day-5-Arrow-Functions");
// const sides = require("./Day-5-Template-Literals");
// const getDayName = require("./Day-6-JavaScript-Dates");
const getMaxLessThanK = require("./Day-6-Bitwise");
// const regexVar = require("./Day-7-Regular-Expressions-I");
// const regexVar = require("./Day-7-Regular-Expressions-II");
// const regexVar = require("./Day-7-Regular-Expressions-III");

("use strict");

process.stdin.resume();
process.stdin.setEncoding("utf-8");

let inputString = "";
let currentLine = 0;

process.stdin.on("data", (inputStdin) => {
  inputString += inputStdin;
});

process.stdin.on("end", (_) => {
  inputString = inputString
    .trim()
    .split("\n")
    .map((string) => {
      return string.trim();
    });

  main();
});

function readLine() {
  return inputString[currentLine++];
}

function main() {
  // Day 0: Hello World
  // const parameterVariable = readLine();
  // greeting(parameterVariable);

  // Day 0: Data Type
  // const secondInteger = readLine();
  // const secondDecimal = readLine();
  // const secondString = readLine();
  // performOperation(secondInteger, secondDecimal, secondString);

  // Day 1: Arithmetric Operators
  // const length = +readLine();
  // const width = +readLine();

  // console.log(Arithmetrics.getArea(length, width));
  // console.log(Arithmetrics.getPerimeter(length, width));

  // Day 1: Functions
  // const n = +readLine();
  // console.log(Factorial.factorial(n));

  // Day 2: Conditional Statements: Switch
  // const s = readLine();
  // console.log(getLetter(s));

  // Day 2: Conditional Statements: If Else
  // const score = +readLine();
  // console.log(getGrade(score));

  // Day 2: Loops
  // const s = readLine();
  // vowelsAndConsonants(s);

  // Day 3: Try, Catch and Finally
  // const s = eval(readLine());
  // reverseString(s);

  // Day 3: Throw
  // const n = +readLine();

  // for (let i = 0; i < n; i++) {
  //   const a = +readLine();

  //   try {
  //     console.log(isPositive(a));
  //   } catch (e) {
  //     console.log(e.message);
  //   }
  // }

  // Day 3: Arrays
  // const n = +readLine();
  // const nums = readLine().split(" ").map(Number);
  // console.log(getSecondLargest(nums));

  // Day 4: Create a Rectangle Object
  // const a = +readLine();
  // const b = +readLine();
  // const rec = new Rectangle(a, b);

  // console.log(rec.length);
  // console.log(rec.width);
  // console.log(rec.perimeter);
  // console.log(rec.area);

  // Day 4: Count Objects
  // const n = +readLine();
  // let objects = [];

  // for (let i = 0; i < n; i++) {
  //   const [a, b] = readLine().split(" ");

  //   objects.push({ x: +a, y: +b });
  // }

  // console.log(getCount(objects));

  // Day 5: Arrow Functions
  // const n = +readLine();
  // const a = readLine().split(" ").map(Number);
  // console.log(modifyArray(a).toString().split(",").join(" "));

  // Day 5: Template Literals
  // let s1 = +(readLine());
  // let s2 = +(readLine());

  // [s1, s2] = [s1, s2].sort();

  // const [x, y] = sides`The area is: ${s1 * s2}.\nThe perimeter is: ${2 * (s1 + s2)}.`;

  // console.log((s1 === x) ? s1 : -1);
  // console.log((s2 === y) ? s2 : -1);

  // Day 6: JavaScript Dates
  // const d = +readLine();

  // for (let i = 0; i < d; i++) {
  //   const date = readLine();

  //   console.log(getDayName(date));
  // }

  // Day 6: Bitwise
  const q = +readLine();

  for (let i = 0; i < q; i++) {
    const [n, k] = readLine().split(" ").map(Number);

    console.log(getMaxLessThanK(n, k));
  }

  // Day 7: Regular Expressions I
  // const re = regexVar();
  // const s = readLine();
  // console.log(re.test(s));

  // Day 7: Regular Expressions II
  // const re = regexVar();
  // const s = readLine();
  // console.log(!!s.match(re));

  // Day 7: Regular Expressions III
  // const re = regexVar();
  // const s = readLine();
  // const r = s.match(re);

  // for (const e of r) {
  //   console.log(e);
  // }
}
