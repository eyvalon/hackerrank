/*
 * Determine the original side lengths and return an array:
 * - The first element is the length of the shorter side
 * - The second element is the length of the longer side
 *
 * Parameter(s):
 * literals: The tagged template literal's array of strings.
 * expressions: The tagged template literal's array of expression values (i.e., [area, perimeter]).
 */
module.exports = function (literals, ...expressions) {
  // Separate area and perimeter from input
  const [area, perimeter] = expressions;

  // Using the formula - P + (sqrt(P^2 - 16*A) / 4)
  const squareRoot = Math.sqrt(perimeter * perimeter - 16 * area);
  const s1 = (perimeter + squareRoot) / 4;
  const s2 = (perimeter - squareRoot) / 4;

  return [s1, s2].sort();
};
