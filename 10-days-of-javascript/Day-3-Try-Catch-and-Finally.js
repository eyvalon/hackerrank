/*
 * Complete the reverseString function
 * Use console.log() to print to stdout.
 */
module.exports = function (s) {
  try {
    // tryCode - Block of code to try
    s = s.split("").reverse().join("");
  } catch (e) {
    // catchCode - Block of code to handle errors
    console.log(e.message);
  } finally {
    // finallyCode - Block of code to be executed regardless of the try / catch result
    console.log(s);
  }

  return s;
};
