/*
 * Create the function factorial here
 */
module.exports = {
  factorial: function (n) {
    if (n == 1) {
      return 1;
    }

    return n * this.factorial(n - 1);
  }
};
