/*
 * Complete the Rectangle function
 */
module.exports = function (a, b) {
  let rectangleObject = new Object();

  rectangleObject.length = a;
  rectangleObject.width = b;
  rectangleObject.area = a * b;
  rectangleObject.perimeter = 2 * (a + b);

  return rectangleObject;
};
