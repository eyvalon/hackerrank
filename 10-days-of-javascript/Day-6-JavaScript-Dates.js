// The days of the week are: "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
module.exports = function (dateString) {
  // Write your code here
  let dayArray = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"
  ];

  // Extracts the current day number from dateString
  const date1 = new Date(dateString);
  const dayName = dayArray[date1.getDay().toString()];

  return dayName;
};
