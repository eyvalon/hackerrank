/*
 * Complete the vowelsAndConsonants function.
 * Print your output using 'console.log()'.
 */
module.exports = function (s) {
  let vowelArray = [];
  let consonantArray = [];
  const vowelList = "aieou";

  for (let i = 0; i < s.length; i++) {
    if (vowelList.indexOf(s[i]) !== -1) {
      vowelArray.push(s[i]);
    } else {
      consonantArray.push(s[i]);
    }
  }

  const finalOutput = vowelArray.concat(consonantArray);
  for (let x = 0; x < finalOutput.length; x++) {
    console.log(finalOutput[x]);
  }
};
