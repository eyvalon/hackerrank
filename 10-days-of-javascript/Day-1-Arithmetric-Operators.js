module.exports = {
  /**
   *   Calculate the area of a rectangle.
   *
   *   length: The length of the rectangle.
   *   width: The width of the rectangle.
   *
   *	Return a number denoting the rectangle's area.
   **/
  getArea: function (length, width) {
    return length * width;
  },

  /**
   *   Calculate the perimeter of a rectangle.
   *
   *	length: The length of the rectangle.
   *   width: The width of the rectangle.
   *
   *	Return a number denoting the perimeter of a rectangle.
   **/
  getPerimeter: function (length, width) {
    return 2 * (length + width);
  }
};
