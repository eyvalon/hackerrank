/*
 * Declare a RegExp object variable named 're'
 * It must match a string that starts and ends with the same vowel (i.e., {a, e, i, o, u})
 */
module.exports = function () {
  let regex = /^([aeiou])(.*)\1$/g;
  let re = new RegExp(regex);

  /*
   * Do not remove the return statement
   */
  return re;
};
