#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the camelcase function below.
def camelcase(s):
	stringArray = []
	currentString = ""
	for i in s:
		currentAscii = ord(i)
		if(currentAscii >= 97) and (currentAscii <= 122):
			currentString += i
		else:
			stringArray.append(currentString)
			currentString = ""
			currentString += i

	stringArray.append(currentString)
	return len(stringArray)

if __name__ == '__main__':
	s = input()
	result = camelcase(s)
	print(result)