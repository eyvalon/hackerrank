#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the minimumNumber function below.
def minimumNumber(n, password):
	# Return the minimum number of characters to make the password strong

	# Default base case
	numbers = "0123456789"
	lower_case = "abcdefghijklmnopqrstuvwxyz"
	upper_case = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	special_characters = "!@#$%^&*()-+"

	# False / True booleans for base case
	checked = [0,0,0,0]

	for i in range(n):
		if password[i] in numbers:
			checked[0] = 1
		elif password[i] in lower_case:
			checked[1] = 1
		elif password[i] in upper_case:
			checked[2] = 1
		elif password[i] in special_characters:
			checked[3] = 1

	count = 0
	for x in checked:
		if x == 0:
			count += 1

	if n >= 6:
		return count
	else:
		if count > (6-n):
			return count
		else:
			return 6-n

if __name__ == '__main__':
	n = int(input())
	password = input()
	answer = minimumNumber(n, password)
	print(answer)