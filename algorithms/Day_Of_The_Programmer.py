#!/bin/python3

import math
import os
import random
import re
import sys

# Number of days per month - Gregorian (February starts on 14th so have 15 days in total)
months_G = {
	"january": 31,
	"february": 15,
	"march": 31,
	"april": 30,
	"may": 31,
	"june": 30,
	"july": 31,
	"august": 31,
	"september": 30,
	"october": 31,
	"november": 30,
	"december": 31
}

# Number of days per month - Julian
months_J = {
	"january": 31,
	"february": 28,
	"march": 31,
	"april": 30,
	"may": 31,
	"june": 30,
	"july": 31,
	"august": 31,
	"september": 30,
	"october": 31,
	"november": 30,
	"december": 31
}

# Complete the dayOfProgrammer function below.
def dayOfProgrammer(year):
	months = months_J

	if(year == 1918):
		months = months_G

	date_to_find = 256
	current_month = 0
	count = 0
	day_remainder = 0

	for month_name, number_of_days in months.items():
		current_month += 1
		
		if (count + number_of_days) > date_to_find:
			day_remainder = (date_to_find - count)
			break
		else:
			count += number_of_days
    
	if (year > 1699) and (year < 1918):
		# Conditions for finding leap year in Julian's calendar
		if (year % 4) == 0:
			day_remainder -= 1 

	else:
		# Conditions for finding leap year in Gregorian's calendar
		if (year % 400) == 0:
			day_remainder -= 1
		elif (year % 4 == 0):
			if not(year % 100 == 0):
				day_remainder -= 1

	return "%02d.%02d.%02d" %(day_remainder, current_month, year)

if __name__ == '__main__':
	year = int(input().strip())
	result = dayOfProgrammer(year)
	print(result)
