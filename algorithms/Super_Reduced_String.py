#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the superReducedString function below.
def calculateString(s):
	currentString = ""

	for i in s:
		# Check before adding
		if(not(len(currentString)) == 0):
			if(currentString[-1] == i):
				currentString = currentString[:-1]
			else:
				currentString += i
		else:
			currentString += i

	return currentString

def superReducedString(s):
	finalString = calculateString(s)

	if len(finalString) == 0:
		return "Empty String"

	return finalString

if __name__ == '__main__':
	s = input()
	result = superReducedString(s)
	print(result)
