/*
 * Complete the 'oddNumbers' function below.
 *
 * The function is expected to return an INTEGER_ARRAY.
 * The function accepts following parameters:
 *  1. INTEGER l
 *  2. INTEGER r
 */
module.exports = function (l, r) {
  let num = [];

  for (let i = l; i < r + 1; i++) {
    if (i % 2 != 0) {
      num.push(i);
    }
  }

  return num;
};
