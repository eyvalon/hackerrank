/*
 * Complete the 'findNumber' function below.
 *
 * The function is expected to return a STRING.
 * The function accepts following parameters:
 *  1. INTEGER_ARRAY arr
 *  2. INTEGER k
 */
module.exports = function (arr, k) {
  let num = "NO";

  for (let i = 0; i < arr.length; i++) {
    if (arr[i] == k) {
      num = "YES";
      break;
    }
  }

  return num;
};
