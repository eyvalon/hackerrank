const findNumber = require("./Find-the-Number");
const oddNumbers = require("./Odd-Numbers");

("use strict");

process.stdin.resume();
process.stdin.setEncoding("utf-8");

let inputString = "";
let currentLine = 0;

process.stdin.on("data", (inputStdin) => {
  inputString += inputStdin;
});

process.stdin.on("end", (_) => {
  inputString = inputString
    .trim()
    .split("\n")
    .map((string) => {
      return string.trim();
    });

  main();
});

function readLine() {
  return inputString[currentLine++];
}

function main() {
  // const arrCount = parseInt(readLine().trim(), 10);
  // let arr = [];

  // for (let i = 0; i < arrCount; i++) {
  //   const arrItem = parseInt(readLine().trim(), 10);
  //   arr.push(arrItem);
  // }

  // const k = parseInt(readLine().trim(), 10);
  // const result = findNumber(arr, k);

  const l = parseInt(readLine().trim(), 10);
  const r = parseInt(readLine().trim(), 10);
  const result = oddNumbers(l, r);

  console.log(result);
}
