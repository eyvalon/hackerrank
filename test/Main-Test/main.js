const maxLength = require("./Max-Subarray-Length");

("use strict");

process.stdin.resume();
process.stdin.setEncoding("utf-8");

let inputString = "";
let currentLine = 0;

process.stdin.on("data", (inputStdin) => {
  inputString += inputStdin;
});

process.stdin.on("end", (_) => {
  inputString = inputString
    .trim()
    .split("\n")
    .map((string) => {
      return string.trim();
    });

  main();
});

function readLine() {
  return inputString[currentLine++];
}

function main() {
  const aCount = parseInt(readLine().trim(), 10);

  let a = [];

  for (let i = 0; i < aCount; i++) {
    const aItem = parseInt(readLine().trim(), 10);
    a.push(aItem);
  }

  const k = parseInt(readLine().trim(), 10);

  const result = maxLength(a, k);
  console.log(result);
}
