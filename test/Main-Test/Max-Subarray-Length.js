/*
 * Complete the 'maxLength' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts following parameters:
 *  1. INTEGER_ARRAY a
 *  2. INTEGER k
 */
module.exports = function (a, k) {
  function getSum(total, num) {
    return total + Math.round(num);
  }

  // Contains the longest value of subarray length it had found
  let previousHighest = 0;

  // Sets the start and end indexes
  let lIndex = 0;
  let rIndex = 0;
  let currentSum = 0;

  // New sub-array to work from
  let newA = a.slice(0, a.length);

  while (rIndex < a.length) {
    newA = a.slice(lIndex, rIndex);
    currentSum = newA.reduce(getSum, 0);

    // A loop that checks left and right (incrementing where it sees fit to expand the current subarray)
    if (currentSum < k) rIndex++;
    else lIndex++;

    // Calculate the new length to see if it's greater than previous length
    currentSum = a.slice(lIndex, rIndex).reduce(getSum, 0) < k;
    if (currentSum)
      if (newA.length > previousHighest) previousHighest = newA.length;
  }

  return previousHighest;
};
