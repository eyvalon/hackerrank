#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'countingValleys' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER steps
#  2. STRING path
#

# Counting how many volley times from bottom to top
def countingValleys(steps, path):
	numOfVolleys = 0
	currentPosition = 0
	for i in range(steps):
		previousPosition = currentPosition

		if path[i] == "D":
			currentPosition -= 1
		else:
			currentPosition += 1

		if (currentPosition == 0) and (previousPosition < 0):
			numOfVolleys += 1

	return numOfVolleys

if __name__ == '__main__':
	steps = int(input().strip())
	path = input()

	result = countingValleys(steps, path)
	print (result)
