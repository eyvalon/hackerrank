#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the jumpingOnClouds function below.
def jumpingOnClouds(c):
	indexPosition = [i for i, e in enumerate(c) if e == 1]
	indexCount = 0
	currentIndex = 0
	stepCounts = 0

	while (currentIndex < len(c) - 1):
		stepCounts += 1
		increment = 0

		if not(indexCount == len(indexPosition)):
			if not((currentIndex + 1) == indexPosition[indexCount]):
				increment = 1

			if not((currentIndex + 2) == indexPosition[indexCount]):
				increment = 2

			if ((currentIndex + 1) == indexPosition[indexCount]) or ((currentIndex + 2) == indexPosition[indexCount]):
				indexCount += 1
		else:
			if ((currentIndex + 1) > len(c)):
				increment = 1
			else:
				increment = 2

		currentIndex += increment

	return stepCounts

if __name__ == '__main__':
	n = int(input())
	c = list(map(int, input().rstrip().split()))

	result = jumpingOnClouds(c)
	print(result)